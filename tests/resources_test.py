import unittest
from caylusMC import Money

class TestResources(unittest.TestCase):

    def test_money_instance(self):
        s1 = Money("Denier", 100)
        s2 = Money("Denier", 100)
        self.assertIs(s1, s2)

    def test_money_number_value(self):
        s1 = Money("Denier", 100)
        name = s1.name
        number = s1.number
        s2 = Money("Denier", 50)
        self.assertEqual( number, s2.number )

    def test_money_name_value(self):
        s1 = Money("Denier1", 100)
        name = s1.name
        number = s1.number
        s2 = Money("Denier2", 100)
        self.assertEqual( name, s1.name )

if __name__ == '__main__':
    unittest.main()