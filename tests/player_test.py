import unittest
from caylusMC import ColorPlayer, Player, \
    Resource

class TestPlayers(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        Player.n_workers = 2
        Player.money_resources = {}
        Player.n_prestige_pts = 0

        cls.food  = Resource( "Food",  100 )
        cls.wood  = Resource( "Wood",  100 )
        cls.stone = Resource( "Stone", 100 )
        cls.gold  = Resource( "Gold",  100 )

    def test_player_resource_all_payments_without_any(self):
        # current_money_resources = 3F,2W,3S,3G and resource_costs = -1F,-3W,(S),-1G requires resource_payments = -1F,-2W,-0S,-1G-1G.
        player = Player( ColorPlayer( "red" ) )
        player_resource = { # type: Dict[Ressource, int]
            self.food: 3,
            self.wood: 2,
            self.stone: 3,
            self.gold: 3
        }
        player.setup()
        player.current_money_resources = player_resource

        resource_costs = { # type: Dict[Ressource, int]
            self.food: -1,
            self.wood: -3,
            self.gold: -1
        }
        
        excepted_result = [{ # type: List[Dict[Ressource, int]]
            self.food: -1,
            self.wood: -2,
            self.stone: 0,
            self.gold: -2
        }]

        result = player.resource_all_payments( resource_costs ) # type: List[Dict[Resource, int]]
        self.assertEqual( len(result), len(excepted_result))
        self.assertDictEqual( result[0], excepted_result[0] )

    def test_player_resource_all_payments_with_any(self):
        # current_money_resources = 3F,2W,3S,3G and resource_costs = -1any,(F),-1W,(S),(G) requires resource_payments = -1F,-1W,-0S,-0G or -0F,-2W,-0S,-0G or -0F,-1W,-1S,-0G (but not -0F,-1W,-0S,-1G).
        player = Player( ColorPlayer( "red" ) ) # type: Player
        player_resource = { # type: Dict[Ressource, int]
            self.food: 3,
            self.wood: 2,
            self.stone: 3,
            self.gold: 3
        }
        player.setup()
        player.current_money_resources = player_resource

        resource_costs = { # type: Dict[Ressource, int]
            None: -1,
            self.wood: -1
        }
        
        excepted_result = [{ # type: List[Dict[Ressource, int]]
            self.food: -1,
            self.wood: -1,
            self.stone: 0,
            self.gold: 0
        },{
            self.food: 0,
            self.wood: -2,
            self.stone: 0,
            self.gold: 0
        },{
            self.food: 0,
            self.wood: -1,
            self.stone: -1,
            self.gold: 0
        }]

        unexcepted_result = { # type : Dict[Resource, int]
            self.food: 0,
            self.wood: -1,
            self.stone: 0,
            self.gold: -1
        }
        result = player.resource_all_payments( resource_costs ) # type: List[Dict[Resource, int]]
        self.assertEqual( len(result), len(excepted_result))
        for x in range(len(excepted_result)):
            self.assertIn( excepted_result[x], result )
            self.assertNotIn( unexcepted_result, result )

if __name__ == '__main__':
    unittest.main()