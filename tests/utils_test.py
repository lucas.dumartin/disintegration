import unittest
from caylusMC import ordinal_number

class TestUtils(unittest.TestCase):

    def test_ordinal_minus_1(self):
        with self.assertRaises(Exception):
            ordinal_number( -1 )

    def test_ordinal_11(self):
        res = ordinal_number(11)
        self.assertRegexpMatches( res, ".*th$")

    def test_ordinal_1(self):
        res = ordinal_number(1)
        self.assertRegexpMatches( res, ".*st$")

    def test_ordinal_22(self):
        res = ordinal_number(22)
        self.assertRegexpMatches( res, ".*nd$")

    def test_ordinal_143(self):
        res = ordinal_number(143)
        self.assertRegexpMatches( res, ".*rd$")

if __name__ == '__main__':
    unittest.main()