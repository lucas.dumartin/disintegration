run:
	python main.py resources/game_elements-CaylusMagnaCarta.xml Beginner red=Basic green orange=Advanced blue=Basic
test:
	python -m unittest discover -s tests -p "*_test.py"

global_uml:
	pyreverse -o pdf caylusMC -k
	mkdir -p uml/
	mv classes.pdf uml/global_simplified_uml.pdf
	rm -f packages.pdf
doc:
	python generate_doc.py