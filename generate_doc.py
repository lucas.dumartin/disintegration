#!/usr/bin/python
# -*- coding: utf-8 -*-
import pydoc
import pkgutil
import os

DOC_PATH="docs/"
PACKAGE_PATH="caylusMC"

def writedoc( arg: str ):
    try:
        object, name = pydoc.resolve(arg, 0)
        page = pydoc.html.page(pydoc.describe(object), pydoc.html.document(object, name))
        with open(DOC_PATH + name + '.html', 'w', encoding='utf-8') as file:
            file.write(page)
        print('wrote', name + '.html')
    except (ImportError, pydoc.ErrorDuringImport) as value:
        print(value)


if __name__ == "__main__":
    print('Starting')
    if not os.path.exists(DOC_PATH):
        os.makedirs(DOC_PATH)
    for importer, modname, ispkg in pkgutil.walk_packages([PACKAGE_PATH], ''):
        writedoc(PACKAGE_PATH + "." + modname)
    writedoc(PACKAGE_PATH)
    print('Done')